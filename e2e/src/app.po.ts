import { browser, by, element, protractor } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getFilterTitle() {
    return element(by.css('body > app-root > div > app-location-filter > div > mat-card > mat-card-title'))
      .getText() as Promise<string>;
  }

  getDialogTitle() {
    return element(by.css('#dialogTitle'))
      .getText() as Promise<string>;
  }

  getNewLocationButton() {
    const elm = element(by.css('#newLocation > span'));
    browser.wait(protractor.ExpectedConditions.presenceOf(elm), 5000);
    return elm;
  }

  getFirstRecordName() {
    // tslint:disable-next-line:max-line-length
    const elm = element(by.css('body > app-root > div > app-location-list > table > tbody > tr:nth-child(1) > td.mat-cell.cdk-column-name.mat-column-name.ng-star-inserted'));
    browser.wait(protractor.ExpectedConditions.presenceOf(elm), 5000);
    return elm;
  }

  fillForm(unique) {
    element(by.css('#name')).sendKeys(unique);
    element(by.css('#normalizedName')).sendKeys('protractortestnormalizedname');
    element(by.css('#latitudeInDegrees')).sendKeys('48.12');
    element(by.css('#longitudeInDegrees')).sendKeys('13.08');
    element(by.css('#city')).sendKeys('Protractor city ');
    element(by.css('#country')).sendKeys('FR');

    element(by.css('#function')).click();
    element(by.css('#mat-option-9 > span')).click();
    element(by.css('#continent')).click();
    element(by.css('#mat-option-15 > span')).click();
    browser.sleep(10000);
    element(by.css('#save')).click();
  }
}
