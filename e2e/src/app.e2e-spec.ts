import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('Should display filter list and the create new location button', () => {
    page.navigateTo();
    expect(page.getFilterTitle()).toEqual('Search Locations');
  });

  it('Should display new location button', () => {
    expect(page.getNewLocationButton().getText()).toEqual('New Location');
  });

  it('Should display New Location form', () => {
    page.getNewLocationButton().click();
    expect(page.getDialogTitle()).toEqual('New Location');
  });

  const unique = 'Protractor: test name ' + new Date().toISOString();

  it('Should Fill and submit the form', () => {
    page.fillForm(unique);
    browser.sleep(10000);
  });

  it('First record should correspond to the inserted record', () => {
    expect(page.getFirstRecordName().getText()).toEqual(unique);
    browser.sleep(20000);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
