import { LocationQueryModel } from './location/models/location-query.model';
import { Component, OnInit } from '@angular/core';
import { LocationTableviewService } from './location/services/location-tableview.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private query: LocationQueryModel;

  constructor(private tableService: LocationTableviewService) {}

  ngOnInit() {
    this.query = {
      name: '',
      function: '',
      id: '',
      paging: {
        pageSize: 30,
        pageIndex: 0,
      },
      sort: []
    };
    this.tableService.getLocations(this.query);
  }

  onPaging($event) {
    console.log($event);
    this.query.paging.pageSize = $event.pageSize;
    this.query.paging.pageIndex = $event.pageIndex;
    this.tableService.getLocations(this.query);
  }

  onSorting($event) {
    console.log($event);
    // reset paging
    this.query.paging.pageIndex = 0;
    this.query.sort = $event;
    this.tableService.getLocations(this.query);
  }

  onFiltering($event) {
    console.log($event);
    // reset paging
    this.query.paging.pageIndex = 0;
    this.query = { ...this.query,
      id: $event.id,
      name: $event.name,
      function: $event.function
    };
    this.tableService.getLocations(this.query);
  }
}
