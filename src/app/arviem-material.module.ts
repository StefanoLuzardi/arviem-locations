import { NgModule } from '@angular/core';
import { MatButtonModule, MatSnackBarModule, MatProgressSpinnerModule,
    MatInputModule, MatTableModule, MatPaginatorModule, MatSortModule,
    MatToolbarModule, MatOptionModule, MatSelectModule, MatCardModule, MatDialogModule } from '@angular/material';


@NgModule({
    imports: [
        MatButtonModule,
        MatSnackBarModule,
        MatProgressSpinnerModule,
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatToolbarModule,
        MatSelectModule,
        MatOptionModule,
        MatButtonModule,
        MatCardModule],
    exports: [
        MatButtonModule,
        MatSnackBarModule,
        MatProgressSpinnerModule,
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatToolbarModule,
        MatSelectModule,
        MatOptionModule,
        MatButtonModule,
        MatCardModule,
        MatDialogModule]
})

export class ArviemMaterialModule {}
