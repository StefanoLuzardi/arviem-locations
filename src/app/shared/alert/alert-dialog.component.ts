import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-alert-dialog',
    templateUrl: './alert-dialog.component.html',
    styleUrls: ['./alert-dialog.component.css']
})

export class AlertDialogComponent {
    private title: string;
    private message: string;

    constructor(
        private dialogRef: MatDialogRef<AlertDialogComponent>,
        @Inject(MAT_DIALOG_DATA) data) {
        this.title = data.title;
        this.message = data.message;
    }

    close() {
        this.dialogRef.close();
    }
}
