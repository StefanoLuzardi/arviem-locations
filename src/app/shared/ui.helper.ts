import { AlertDialogComponent } from './alert/alert-dialog.component';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { MatSnackBar, MatDialog, MatDialogConfig } from '@angular/material';

@Injectable()
export class UIHelper {
  loadingState = new Subject<boolean>();

  constructor(private snackbar: MatSnackBar,
              private dialog: MatDialog) {}

  showMessage(message, action, time) {
    this.snackbar.open(message, action, {
        duration: time
    });
  }

  showAlert(title: string, message: string) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    dialogConfig.data = {
        title,
        message
    };
    this.dialog.open(AlertDialogComponent, dialogConfig);
  }
}
