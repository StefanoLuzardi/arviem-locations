import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { ArviemMaterialModule } from '../arviem-material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ArviemMaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    ArviemMaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule
  ]
})
export class SharedModule {}
