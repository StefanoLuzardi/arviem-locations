import { Action } from '@ngrx/store';

export const START_LOADING = 'UI_LOADING_START';
export const STOP_LOADING = 'UI_LOADING_STOP';

export class StartLoading implements Action {
  readonly type = START_LOADING;
}

export class StopLoading implements Action {
  readonly type = STOP_LOADING;
}

export type UIActions = StartLoading | StopLoading;
