import { LocationModule } from './location/location.module';
import { AlertDialogComponent } from './shared/alert/alert-dialog.component';
import { UIHelper } from './shared/ui.helper';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { ArviemMaterialModule } from './arviem-material.module';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { reducers } from './app.reducer';
import { AppRoutingModule } from './app.roting.module';

@NgModule({
  declarations: [
    AppComponent,
    AlertDialogComponent
  ],
  imports: [
    LocationModule,
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    StoreModule.forRoot(reducers),
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [UIHelper],
  bootstrap: [AppComponent],
  entryComponents: [AlertDialogComponent]
})
export class AppModule { }
