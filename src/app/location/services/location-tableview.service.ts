import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { Subject, Observable, throwError } from 'rxjs';
import { LocationQueryModel } from '../models/location-query.model';
import { LocationResultsModel } from '../models/location-results.model';
import { Location } from '../models/location.model';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { UIHelper } from '../../shared/ui.helper';
import * as fromRoot from '../../shared/ui.redcer';
import * as UI from '../../shared/ui.actions';
import { LocationService } from './locations.service';

@Injectable({ providedIn: 'root' })
export class LocationTableviewService {
    locationChanged = new Subject<LocationResultsModel>();
    APIURL = environment.APIUrl;
    constructor(private locService: LocationService,
                private uiHelper: UIHelper,
                private store: Store<fromRoot.State>
                ) { }

    getLocations(query: LocationQueryModel) {
        this.store.dispatch(new UI.StartLoading());
        this.locService.search(query)
        .pipe(
            map(data => {
                const res: LocationResultsModel = {
                    length: query.paging.pageIndex * query.paging.pageSize +
                        (data.length === query.paging.pageSize ?
                        query.paging.pageSize * 2 :
                        data.length),
                    pageIndex: query.paging.pageIndex,
                    pageSize: query.paging.pageSize,
                    results: data
                };
                return res;
            })
        )
        .subscribe(data => {
            this.uiHelper.showMessage('Locations fetched from server', null, 2500);
            this.locationChanged.next(data);
            this.store.dispatch(new UI.StopLoading());
        },
        (error) => {
            this.store.dispatch(new UI.StopLoading());
            this.uiHelper.showAlert('Error', error.message);
        });
    }
    addLocation(location: Location): Observable<Location> {
        return this.locService.addLocation(location);
    }

    updateLocation(location: Location): Observable<Location> {
        return this.locService.updateLocation(location);
    }

    normalizeResourceId(resourceId: string) {
        const pos = resourceId.lastIndexOf('/');
        return resourceId.substr(pos + 1);
    }

    denormalizeResourceId(id: string) {
        return '/tenant1/locations/' + id;
    }

}
