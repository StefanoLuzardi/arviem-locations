import { environment } from '../../../environments/environment';
import { LocationService } from 'src/app/location/services/locations.service';

import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClientModule, HttpRequest, HttpParams } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('GithubApiService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientModule, HttpClientTestingModule],
            providers: [LocationService]
        });
    });

    afterEach(inject([HttpTestingController], (backend: HttpTestingController) => {
        backend.verify();
      }));

    it('should be created', () => {
        const ser: LocationService = TestBed.get(LocationService);
        expect(ser).toBeTruthy();
    });
    describe('#search', () => {
        it('should return an Observable<SearchResults> with correct page size = 30',
        async(inject([LocationService, HttpTestingController],
            (service: LocationService, backend: HttpTestingController) => {
            service.search({
                name: null,
                function: null,
                id: null,
                paging: { pageSize: 30, pageIndex: 0 },
                sort: []
            })
                .subscribe(next => {
                    alert(next);
                });
            backend.expectOne((req: HttpRequest<any>) => {
                    return req.method === 'GET'
                        && req.urlWithParams === `${environment.APIUrl}?page=0&pageSize=30`;
                });

        })));

        it('should return an Observable<SearchResults> with correct page size = 50',
        async(inject([LocationService, HttpTestingController],
            (service: LocationService, backend: HttpTestingController) => {
            service.search({
                name: null,
                function: null,
                id: null,
                paging: { pageSize: 50, pageIndex: 0 },
                sort: []
            })
                .subscribe(next => {});
            backend.expectOne((req: HttpRequest<any>) => {
                // alert(req.urlWithParams);
                // alert(`${environment.APIUrl}?page=0&pageSize=50`);
                return req.method === 'GET'
                        && req.urlWithParams === `${environment.APIUrl}?page=0&pageSize=50`;
                });
        })));

        it('should compose correct sort parameters',
        async(inject([LocationService, HttpTestingController],
            (service: LocationService, backend: HttpTestingController) => {
            service.search({
                name: null,
                function: null,
                id: null,
                paging: { pageSize: 30, pageIndex: 0 },
                sort: ['name', '-function']
            })
                .subscribe(next => {});
            backend.expectOne((req: HttpRequest<any>) => {

                return req.method === 'GET'
                        && req.urlWithParams === `${environment.APIUrl}?page=0&pageSize=30&sort=name,-function`;
                });
        })));
    });
});
