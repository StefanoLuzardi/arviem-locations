import { environment } from '../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { LocationQueryModel } from '../models/location-query.model';
import { Location } from '../models/location.model';
import { map, retry, catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class LocationService {
    APIURL = environment.APIUrl;
    constructor(private http: HttpClient) { }

    search(query: LocationQueryModel): Observable<Location[]> {
        return this.http
            .get<Location[]>(this.APIURL + this.prepareApiUrl(query))
            .pipe(
                map((data: Location[]) => {
                    return data.map((location: Location) => {
                        location.resourceId = this.normalizeResourceId(location.resourceId);
                        return location;
                    });
                })
            );
    }

    getLocation(resourceId: string): Observable<Location> {
        return this.search({id: resourceId, name: null, function: null, paging: null, sort: null})
        .pipe(
            map((locations: Location[]) => {
                if (locations && locations.length > 0) {
                    return locations[0];
                } else {
                    return null;
                }
            })
        );
    }

    addLocation(location: Location): Observable<Location> {
        return this.http.post<Location>(this.APIURL, location)
        .pipe(
            retry(1),
            catchError(this.handleError)
        );
    }

    updateLocation(location: Location): Observable<Location> {
        const updateUrl = this.APIURL + '/' + location.resourceId;
        location = { ...location,
            resourceId: this.denormalizeResourceId(location.resourceId)};
        return this.http.put(updateUrl, location)
            .pipe(
                retry(1),
                catchError(this.handleError)
            ) as Observable<Location>;
    }

    normalizeResourceId(resourceId: string) {
        const pos = resourceId.lastIndexOf('/');
        return resourceId.substr(pos + 1);
    }

    denormalizeResourceId(id: string) {
        return '/tenant1/locations/' + id;
    }

    handleError(error) {
        let errorMessage = '';
        if (error.error && error.error.fieldErrors) {
          // manage every field error
          errorMessage =  `Error Code: ${error.status}\n`;
          error.error.fieldErrors.forEach(flderr => {
            errorMessage += flderr.message + '\n';
          });
        } else {
          // unknown error
          errorMessage += error.message;
        }
        console.log(errorMessage);
        return throwError({...error, message: errorMessage});
      }

    // Change with usage of parameters
    prepareApiUrl(query: LocationQueryModel) {
        let ret = '';
        if (query) {
            if (query.name) {
                ret = 'name=' + encodeURI(query.name);
            }
            if (query.function) {
                ret += (ret !== '' ? '&' : '')
                    + 'function=' + encodeURI(query.function);
            }
            if (query.id) {
                ret += (ret !== '' ? '&' : '')
                    + 'id=' + encodeURI(query.id);
            }
            if (query.paging) {
                ret += (ret !== '' ? '&' : '')
                    + 'page=' + query.paging.pageIndex
                    + '&pageSize=' + query.paging.pageSize;
            }
            if (query.sort && query.sort.length > 0) {
                ret += (ret !== '' ? '&' : '')
                    + 'sort=';
                ret += query.sort.join(',');
            }
            ret = ret !== '' ? '?' + ret : ret;
        }
        return ret;
    }
}
