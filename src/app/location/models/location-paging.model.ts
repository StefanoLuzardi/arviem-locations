export interface LocationPagingModel {
    pageSize: number;
    pageIndex: number;
}
