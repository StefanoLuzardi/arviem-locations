import { LocationPagingModel } from './location-paging.model';

export interface LocationQueryModel {
    name: string;
    function: string;
    id: string;
    paging: LocationPagingModel;
    sort: string[];
}
