import { Location } from './location.model';

export class LocationResultsModel {
    length: number;
    pageIndex: number;
    pageSize: number;
    results: Location[];
}
