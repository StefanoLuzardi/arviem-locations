export interface Address {
    city: string;
    country: string;
    continent: string;
    streetName: string;
    streetNumber: string;
    postalCode: string;
    administrativeLevel2: string;
    administrativeLevel1: string;
}

export interface Coordinates {
    latitudeInDegrees: number;
    longitudeInDegrees: number;
}

export class Location {
    name: string;
    normalizedName: string;
    coordinates: Coordinates;
    address: Address;
    resourceId: string;
    version: number;
    function: string;
}
