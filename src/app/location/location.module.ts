import { NgModule } from '@angular/core';
import { LocationListComponent } from './components/list/location-list.component';
import { LocationFilterComponent } from './components/filter/location-filter.component';
import { SharedModule } from '../shared/shared.module';
import { LocationDetailComponent } from './components/detail/location.detail.component';
import { LocationComponent } from './location.component';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
    path: '',
    component: LocationComponent
}];

@NgModule({
  declarations: [
    LocationListComponent,
    LocationFilterComponent,
    LocationDetailComponent,
    LocationComponent
  ],
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  exports: [],
  entryComponents: [LocationDetailComponent]
})

export class LocationModule { }
