import { LocationQueryModel } from '../../models/location-query.model';
import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-location-filter',
  templateUrl: './location-filter.component.html',
  styleUrls: ['./location-filter.component.css']
})

export class LocationFilterComponent implements OnInit, OnDestroy {
  @Output() Filtering = new EventEmitter<LocationQueryModel>();

  filterForm: FormGroup;

  constructor() {
  }

  ngOnInit() {
    this.filterForm = new FormGroup({
      id: new FormControl('', {
        validators: []
      }),
      name: new FormControl('', {
        validators: []
      }),
      function: new FormControl('', { validators: [] })
    });  }

  ngOnDestroy() {
  }

  filter() {
    console.log('filtering');
    this.Filtering.emit({
      name: this.filterForm.value.name,
      function: this.filterForm.value.function,
      id: this.filterForm.value.id,
      paging: null,
      sort: null
    });
  }
}
