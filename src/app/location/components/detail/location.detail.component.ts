import { Location } from '../../models/location.model';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UIHelper } from 'src/app/shared/ui.helper';
import { LocationService } from 'src/app/location/services/locations.service';

@Component({
    selector: 'app-location-detail',
    templateUrl: './location-detail.component.html',
    styleUrls: ['./location-detail.component.css']
})

export class LocationDetailComponent implements OnInit {
    locationForm: FormGroup;
    coordinatesForm: FormGroup;
    addressForm: FormGroup;
    private title: string;
    private location: Location;

    constructor(
        private fb: FormBuilder,
        private dialogRef: MatDialogRef<LocationDetailComponent>,
        private locService: LocationService,
        private uiHelper: UIHelper,
        @Inject(MAT_DIALOG_DATA) data) {

        this.title = data.title;
        this.location = data.location;
    }

    ngOnInit() {
        this.addressForm = this.fb.group({
            streetName: new FormControl('', {
                validators: [],
                updateOn: 'blur'
            }),
            streetNumber: new FormControl('', {
                validators: [
                    Validators.maxLength(10)
                ],
                updateOn: 'blur'
            }),
            postalCode: new FormControl('', {
                validators: [
                    Validators.maxLength(18)
                ],
                updateOn: 'blur'
            }),
            city: new FormControl('', {
                validators: [
                    Validators.required,
                    Validators.maxLength(60)
                ],
                updateOn: 'blur'
            }),
            country: new FormControl('', {
                validators: [
                    Validators.required,
                    Validators.maxLength(2)
                ],
                updateOn: 'blur'
            }),
            administrativeLevel1: new FormControl('', {
                validators: [
                    Validators.maxLength(60)
                ],
                updateOn: 'blur'
            }),
            administrativeLevel2: new FormControl('', {
                validators: [
                    Validators.maxLength(60)
                ],
                updateOn: 'blur'
            }),
            continent: new FormControl('', {
                validators: [
                    Validators.required
                ],
                updateOn: 'blur'
            })

        });
        this.coordinatesForm = this.fb.group({
            latitudeInDegrees: new FormControl('', {
                validators: [
                    Validators.required,
                    Validators.max(90),
                    Validators.min(-90),
                ],
                updateOn: 'blur'
            }),
            longitudeInDegrees: new FormControl('', {
                validators: [
                    Validators.required,
                    Validators.max(180),
                    Validators.min(-180),
                ],
                updateOn: 'blur'
            })
        });
        this.locationForm = this.fb.group({
            resourceId: new FormControl('', []),
            version: new FormControl('', []),
            name: new FormControl('', {
                validators: [
                    Validators.required,
                    Validators.minLength(1),
                    Validators.maxLength(60),
                ],
                updateOn: 'blur'
            }),
            normalizedName: new FormControl('', {
                validators: [
                    Validators.required,
                    Validators.minLength(1),
                    Validators.maxLength(60),
                    // only lower case no diacritics
                    Validators.pattern('^[0-9a-z]*$'),
                ],
                updateOn: 'blur'
            }),
            function: new FormControl('', {
                validators: [
                    Validators.required
                ],
                updateOn: 'blur'
            }),
            coordinates: this.coordinatesForm,
            address: this.addressForm
        });
        if (this.location) {
            this.locationForm.setValue(this.location);
        }
    }

    save() {
        console.log(this.locationForm.value);
        this.validateFormGroup(this.locationForm);
        if (this.locationForm.valid) {
            const cleanLocation = this.cleanEmptyStrings(this.locationForm.value) as Location;
            if (this.location && this.location.resourceId) {
                this.locService.updateLocation(cleanLocation) // this.locationForm.value)
                    .subscribe((data) => {
                        this.dialogRef.close(true);
                    },
                        error => {
                            this.uiHelper.showAlert('Error updating location', error.message);
                        });
            } else {
                // insert new location
                this.locService.addLocation(cleanLocation)
                    .subscribe((data) => {
                        this.dialogRef.close(true);
                    },
                        error => {
                            this.uiHelper.showAlert('Error inserting location', error.message);
                        });
            }
        } else {
            this.uiHelper.showMessage('check that you have correctly filled out the form', null, 3000);
        }
    }

    close() {
        this.dialogRef.close(false);
    }

    private validateFormGroup(formGroup: FormGroup) {
        Object.values(formGroup.controls).forEach((control: any) => {
            control.markAsTouched();
            if (control.controls) {
                this.validateFormGroup(control);
            }
        });
    }

    private cleanEmptyStrings(value: any): any {
        return Object.keys(value).reduce((acc, key) => {
            if (value[key] !== null) {
                if (typeof (value[key]) === 'object') {
                    acc[key] = this.cleanEmptyStrings(value[key]);
                } else {
                    acc[key] = value[key] === '' ? null : value[key];
                }
            }
            return acc;
        }, {});
    }
}
