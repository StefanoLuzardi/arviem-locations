import { LocationTableviewService } from '../../services/location-tableview.service';
import { LocationDetailComponent } from '../detail/location.detail.component';
import { Location } from '../../models/location.model';
import { LocationResultsModel } from '../../models/location-results.model';
import { Component, OnInit, OnDestroy, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { Subscription, Observable } from 'rxjs';

import { LocationService } from '../../services/locations.service';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../../app.reducer';
import { LocationPagingModel } from 'src/app/location/models/location-paging.model';
import { MatTableDataSource, MatSort, MatDialog, MatDialogConfig } from '@angular/material';

@Component({
    selector: 'app-location-list',
    templateUrl: './location-list.component.html',
    styleUrls: ['./location-list.component.css']
})

export class LocationListComponent implements OnInit, OnDestroy {
    @Output() Paging = new EventEmitter<LocationPagingModel>();
    @Output() Sorting = new EventEmitter<string[]>();
    @ViewChild(MatSort, { static: true }) sorting: MatSort;

    locationList: LocationResultsModel = { length: 0, pageIndex: 1, pageSize: 0, results: [] };
    dataSource = new MatTableDataSource();
    isLoading$: Observable<boolean>;
    private locationListSubs: Subscription;
    public displayedColumns: string[] = ['id', 'function', 'name', 'actions'];

    constructor(private locService: LocationTableviewService,
                private store: Store<fromRoot.State>,
                private dialog: MatDialog) {
    }

    ngOnInit() {
        this.isLoading$ = this.store.select(fromRoot.getIsLoading);
        this.locationListSubs = this.locService.locationChanged.subscribe(locations => {
            this.locationList = locations;
            this.dataSource.data = locations.results;
            this.dataSource.sort = this.sorting;
        });
    }

    ngOnDestroy() {
        this.locationListSubs.unsubscribe();
    }

    changePage($event) {
        console.log($event);
        this.Paging.emit(
            { pageSize: $event.pageSize, pageIndex: $event.pageIndex }
        );
    }

    sort($event) {
        console.log($event);
        // Mat table natively supports only one column sorting
        this.Sorting.emit([
            ($event.direction === 'desc' ? '-' : '')
            + $event.active
        ]);
    }

    newLocation() {
        this.openDetailDialog(null)
            .subscribe(response => {
                if (response === true) {
                    this.sorting.sort({
                        id: 'id',
                        start: 'desc',
                        disableClear: true,
                    });
                }
            });
    }

    updateLocation(loc: Location) {
        this.openDetailDialog(loc)
            .subscribe(response => {
                if (response === true) {
                    this.Paging.emit(
                        {
                            pageSize: this.locationList.pageSize,
                            pageIndex: this.locationList.pageIndex
                        }
                    );
                }
            });
    }

    openDetailDialog(loc: Location): Observable<any> {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;

        dialogConfig.data = {
            location: loc,
            title: loc ? 'Edit location ' + loc.resourceId :
                'New Location'
        };
        return this.dialog.open(LocationDetailComponent, dialogConfig)
            .afterClosed();
    }
}
