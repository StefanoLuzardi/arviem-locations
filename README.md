# arviem-locator
This is the test project of a simple location management.

## Prerequisitie
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.2.

## start-up
After the repository clone launch the command npm install
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## end-to-end tests

One e2e test is implemented:
It creates a new location and, after dialog confirmation, verifies that the location is the first record on the resulting table view
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


